# Python ORM Talk

This repository is meant to house the code and the presentation for a talk on Python ORMs.

The goal of the talk is to introduce developers to object-relational mappers in Python for use with relational databases. This talk will not go into details on how to use Python with non-relational databases.

These are the ORMs we will be discussing:

1. Django ORM
2. SQLAlchemy
3. Peewee
4. Tortoise
5. Pony

The date for the talk is May 7, 2020.
